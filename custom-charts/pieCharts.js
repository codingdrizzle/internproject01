var pieChart1 = document.getElementById('pie-chart-1').getContext('2d');
var Pie1 = new Chart(pieChart1,{
    type : 'pie',
    data : {
        labels: [],
        datasets: [{
            label: '',
            backgroundColor: ['rgba(82,182,223,255)','rgba(104,191,227,255)','rgba(82,182,223,255)','rgba(82,182,223,255)'],
            data: [160, 80, 60, 60],
        }]
    },
    options : {}
})

var pieChart2 = document.getElementById('pie-chart-2').getContext('2d');
var Pie2 = new Chart(pieChart2,{
    type : 'pie',
    data : {
        labels: [],
        datasets: [{
            label: '',
            backgroundColor: ['rgba(112,155,231,255)','rgba(112,155,231,255)','rgba(112,155,231,255)','rgba(99,141,221,255)'],
            data: [120, 120, 60, 60, ],
        }]
    },
    options : {}
})

var pieChart3 = document.getElementById('pie-chart-3').getContext('2d');
var Pie3 = new Chart(pieChart3,{
    type : 'pie',
    data : {
        labels: [],
        datasets: [{
            label: '',
            backgroundColor: ['rgba(123,118,227,255)','rgba(111,101,236,255)','rgba(153,148,242,255)','rgba(136,132,232,255)'],
            data: [110, 130, 60, 60, ],
        }]    
    },    
    options : {}
})    

var pieChart4 = document.getElementById('pie-chart-4').getContext('2d');

var Pie4 = new Chart(pieChart4,{
    type : 'pie',
    data : {
        labels: [],
        datasets: [{
            label: '',
            backgroundColor: ['rgba(164,124,238,255)','rgba(151,103,234,255)','rgba(136,85,225,255)','rgba(172,137,241,255)'],
            data: [80, 80, 60, 140, ],
        }]    
    },    
    options : {}
})    

var pieChart3 = document.getElementById('pie-chart-3').getContext('2d');
var Pie3 = new Chart(pieChart3,{
    type : 'pie',
    data : {
        labels: [],
        datasets: [{
            label: '',
            backgroundColor: ['rgba(123,118,227,255)','rgba(111,101,236,255)','rgba(153,148,242,255)','rgba(136,132,232,255)'],
            data: [110, 130, 60, 60, ],
        }]    
    },    
    options : {}
})
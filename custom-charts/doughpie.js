var pieChart0 = document.getElementById('pie-chart-0').getContext('2d');
var Pie0 = new Chart(pieChart0,{
    type : 'pie',
    data : {
        labels: ['Lithuania: 32.6%','Czechia: 19.6%','Ireland: 13.1%','Germany: 10.8%','Australia: 9.1%','Austria: 8.3%', 'UK: 6.4%'],
        datasets: [{
            label: '',
            backgroundColor: ['rgba(80,183,224,255)','rgba(98,143,226,255)','rgba(109,100,227,255)','rgba(136,83,225,255)','rgba(176,81,225,255)','rgba(215,77,225,255)','rgba(215,77,225,255)'],
            data: [117.36, 70.56, 47.16, 38.88, 32.76, 29.88, 23.04],
        }]    
    },    
    options : {}
})    

var doughnut0 = document.getElementById('doughnut-chart-0').getContext('2d');
let gradough1 = lineChart2.createLinearGradient(0,100,0,200);
let gradough2 = lineChart2.createLinearGradient(0,200,0,100);
gradough1.addColorStop(0, "rgba(252,167,22,255)");
gradough1.addColorStop(1, "rgba(246,188,80,255)");
gradough2.addColorStop(0, "rgba(133,208,38,255)");
gradough2.addColorStop(1, "rgba(184,225,131,255)");

var nut0 = new Chart(doughnut0,{
    type : 'doughnut',
    data : {
        labels: ['73.1 kW','73.1 kW'],
        datasets: [{
            label: '',
            backgroundColor: [gradough1, gradough2],
            data: [240,120],
        }]    
    },    
    options : { 
        cutoutPercentage : 60,
         animation : {
             animateScale : true
         }
    }
})  
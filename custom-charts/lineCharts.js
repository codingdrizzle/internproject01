var lineChart1 = document.querySelector("#line-chart-1").getContext('2d');
let gradient1 = lineChart1.createLinearGradient(0,0,500,0);
gradient1.addColorStop(0, "rgba(191,220,227,255)");
gradient1.addColorStop(1, "rgba(0, 110, 255,255)");

var line1 = new Chart(lineChart1, {
    type : 'line',
    data : {
        labels : ['','','','','','','','','',''],
        datasets : [{
            label: '',
            data: [7, 4, 6, 5, 2, 3, 1,5,7, 4],
            fill: false,
            borderWidth: 1,
            borderColor: gradient1,
            tension: 0.5,
            pointBorderWidth: 0,
            pointBorderColor: gradient1,
        }]
    },
    options: {
        plugins: {
            legend: {
              display: false
            }
        },
        scales: {
            xAxes: [{
               gridLines: {
                  display: true
               }
            }],
            yAxes: [{
               ticks: {
                  beginAtZero: true, 
               },
               gridLines: {
                  display: true
               }
            }]
       },
        tooltips: {
            callbacks: {
               label: function(tooltipItem) {
                      return tooltipItem.yLabel;
               }
            }
        },
        responsive : true,
    }
})

var lineChart2 = document.querySelector("#line-chart-2").getContext('2d');
let gradient2 = lineChart2.createLinearGradient(0,0,500,0);
gradient2.addColorStop(0, "rgba(0, 110, 255,255)");
gradient2.addColorStop(1, "rgba(100,142,226,255)");

var line2 = new Chart(lineChart2, {
    type : 'line',
    data : {
        labels : ['','','','','','','','','',''],
        datasets : [{
            label: '',
            data: [6, 2, 7, 5,1, 1, 4,0,3, 6],
            fill: false,
            borderWidth: 1,
            borderColor: gradient2,
            tension: 0.5,
            pointBorderWidth: 0,
            pointBorderColor: 'transparent',
        }]
    },
    options: {
        plugins: {
            legend: {
              display: false
            }
        },
        scales: {
            xAxes: [{
               gridLines: {
                  display: true
               }
            }],
            yAxes: [{
               ticks: {
                  beginAtZero: true, 
               },
               gridLines: {
                  display: true
               }
            }]
       },
        tooltips: {
            callbacks: {
               label: function(tooltipItem) {
                      return tooltipItem.yLabel;
               }
            }
        },
        responsive : true,
    }
})

var lineChart3 = document.querySelector("#line-chart-3").getContext('2d');
let gradient3 = lineChart3.createLinearGradient(0,0,500,0);
gradient3.addColorStop(0, "rgba(100,142,226,255)");
gradient3.addColorStop(1, "rgba(111,102,228,255)");

var line3 = new Chart(lineChart3, {
    type : 'line',
    data : {
        labels : ['','','','','','','','','',''],
        datasets : [{
            label: '',
            data: [5, 3, 1, 5, 1, 3, 6,2,0,1],
            fill: false,
            borderWidth: 1,
            borderColor: gradient3,
            tension: 0.5,
            pointBorderWidth: 0,
            pointBorderColor: 'transparent',
        }]
    },
    options: {
        plugins: {
            legend: {
              display: false
            }
        },
        scales: {
            xAxes: [{
               gridLines: {
                  display: true
               }
            }],
            yAxes: [{
               ticks: {
                  beginAtZero: true, 
               },
               gridLines: {
                  display: true
               }
            }]
       },
        tooltips: {
            callbacks: {
               label: function(tooltipItem) {
                      return tooltipItem.yLabel;
               }
            }
        },
        responsive : true,
    }
})

var lineChart4 = document.querySelector("#line-chart-4").getContext('2d');
let gradient4 = lineChart4.createLinearGradient(0,0,500,0);
gradient4.addColorStop(0, "rgba(111,102,228,255)");
gradient4.addColorStop(1, "rgba(136,83,225,255)");
var line4 = new Chart(lineChart4, {
    type : 'line',
    data : {
        labels : ['','','','','','','','','',''],
        datasets : [{
            label: '',
            data: [0, 5, 1, 3, 5, 7, 2,1,8, 2],
            fill: false,
            borderWidth: 1,
            borderColor: gradient4,
            tension: 0.5,
            pointBorderWidth: 0,
            pointBorderColor: 'transparent',
        }]
    },
    options: {
        plugins: {
            legend: {
              display: false
            }
        },
        scales: {
            xAxes: [{
               gridLines: {
                  display: true
               }
            }],
            yAxes: [{
               ticks: {
                  beginAtZero: true, 
               },
               gridLines: {
                  display: true
               }
            }]
       },
        tooltips: {
            callbacks: {
               label: function(tooltipItem) {
                      return tooltipItem.yLabel;
               }
            }
        },
        responsive : true,
    }
})
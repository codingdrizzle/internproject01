var performanceChart1 = document.querySelector("#performance-chart-1").getContext('2d');

var perf1 = new Chart(performanceChart1, {
    type : 'line',
    data : {
        labels : ['','','','','','','','','','',''],
        datasets : [{
            label: 'Battery',
            data: [6, 2, 10, 6, 3, 10, 2, 8, 1, 8, 0, 9, 2, 9, 1, 7, 2, 9, 3, 10, 8],
            fill: false,
            borderWidth: 3,
            borderColor: 'rgba(243,180,66,255)',
            backgroundColor: 'rgba(243,180,66,255)',
            tension: 0.5,
            pointBorderWidth: 3,
            pointBorderColor: 'rgba(243,180,66,255)',
            order : 1
        },
        {
            label: 'Utility/Grid',
            data: [5.3, 1.3, 9.3, 5.3, 2.3, 9.3, 1.3, 7.3, 0.3, 7.3, 0, 8.3, 1.3, 8.3, 0.3, 6.3, 1.3, 8.3, 2.3, 9.3, 7.3],
            fill: false,
            borderWidth: 3,
            borderColor: 'rgba(175,218,236,255)',
            backgroundColor: 'rgba(175,218,236,255)',
            tension: 0.5,
            pointBorderWidth: 3,
            pointBorderColor: 'rgba(175,218,236,255)',
            order : 2
        }],
    },
    options: {},
})
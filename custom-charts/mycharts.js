
var batteryChart = document.getElementById('battery-chart').getContext('2d');
var Chart1a = new Chart(batteryChart,{
    type : 'line',
    data : {
        labels: ['battery-1', 'battery-2', 'battery-2', 'battery-2', 'battery-5', 'battery-6'],
        datasets: [{
            label: 'Battery values',
            backgroundColor: 'rgba(153,153,153,255)',
            borderColor: 'rgba(153,153,153,255)',
            data: [2, 7, 9, 12, 20, 21, 23],
            borderWidth: 1
        }]
    },
    options : {}
})
var modalbatteryChart = document.getElementById('modal-battery-chart').getContext('2d');
var Chart1b = new Chart(modalbatteryChart,{
    type : 'line',
    data : {
        labels: ['battery-1', 'battery-2', 'battery-2', 'battery-2', 'battery-5', 'battery-6'],
        datasets: [{
            label: 'Battery values',
            backgroundColor: 'rgba(153,153,153,255)',
            borderColor: 'rgba(153,153,153,255)',
            data: [1, 7, 9, 12, 20, 21, 23],
            borderWidth: 1
        }]
    },
    options : {}
})

var utilityChart = document.getElementById('utility-chart').getContext('2d');
var Chart2a = new Chart(utilityChart,{
    type : 'line',
    data : {
        labels: ['util-1', 'util-2', 'util-2', 'util-2', 'util-5', 'util-6'],
        datasets: [{
            label: 'Grid values',
            backgroundColor: 'rgba(246,179,66,255)',
            borderColor: 'rgba(246,179,66,255)',
            data: [0, 1, 9, 5, 13, 17, 23],
            borderWidth: 1
        }]
    },
    options : {}
})
var modalutilityChart = document.getElementById('modal-utility-chart').getContext('2d');
var Chartb = new Chart(modalutilityChart,{
    type : 'line',
    data : {
        labels: ['util-1', 'util-2', 'util-2', 'util-2', 'util-5', 'util-6'],
        datasets: [{
            label: 'Grid values',
            backgroundColor: 'rgba(246,179,66,255)',
            borderColor: 'rgba(246,179,66,255)',
            data: [0, 1, 9, 5, 13, 17, 23],
            borderWidth: 1
        }]
    },
    options : {}
})

var loadChart = document.getElementById('load-chart').getContext('2d');
var Chart3a = new Chart(loadChart,{
    type : 'line',
    data : {
        labels: ['load-1', 'load-2', 'load-2', 'load-2', 'load-5', 'load-6','load-7', 'load-8', 'load-9', 'load-10', 'load-11', 'load-12'],
        datasets: [{
            label: 'Load values',
            backgroundColor: 'rgba(176,217,235,255)',
            borderColor: 'rgba(176,217,235,255)',
            data: [5, 1, 7, 10, 6, 8, 7.6, 7.9, 8, 9, 17, 7, 9.7, 3],
            borderWidth: 1
        }]
    },
    options : {}
})
var modalloadChart = document.getElementById('modal-load-chart').getContext('2d');
var Chart3b = new Chart(modalloadChart,{
    type : 'line',
    data : {
        labels: ['load-1', 'load-2', 'load-2', 'load-2', 'load-5', 'load-6','load-7', 'load-8', 'load-9', 'load-10', 'load-11', 'load-12'],
        datasets: [{
            label: 'Load values',
            backgroundColor: 'rgba(176,217,235,255)',
            borderColor: 'rgba(176,217,235,255)',
            data: [5, 1, 7, 10, 6, 8, 7.6, 7.9, 8, 9, 17, 7, 9.7, 3],
            borderWidth: 1
        }]
    },
    options : {}
})

var doughNut1 = document.getElementById('doughnut-chart-1').getContext('2d');
var Chart4 = new Chart(doughNut1,{
    type : 'doughnut',
    data : {
        labels: ['73.1 kW', '73.1 kW'],
        datasets: [{
            label: '',
            backgroundColor: ['rgba(246, 180, 61, 255)', 'rgba(176, 217, 235, 255)'],
            data: [270, 90],
        }]
    },
    options : { 
        cutoutPercentage : 60,
         animation : {
             animateScale : true
         }
    }
})

var doughNut2 = document.getElementById('doughnut-chart-2').getContext('2d');
var Chart5 = new Chart(doughNut2,{
    type : 'doughnut',
    data : {
        labels: ['73.1 kW', '73.1 kW'],
        datasets: [{
            label: '',
            backgroundColor: ['rgba(181, 227, 134, 255)', 'rgba(176, 217, 235, 255)'],
            data: [0, 360],
        }]
    },
    options : { 
        cutoutPercentage : 60,
         animation : {
             animateScale : true
         }
    }
})

var doughNut3 = document.getElementById('doughnut-chart-3').getContext('2d');
var Chart6 = new Chart(doughNut3,{
    type : 'doughnut',
    data : {
        labels: ['73.1 kW', '73.1 kW'],
        datasets: [{
            label: '',
            backgroundColor: ['rgba(181, 227, 134, 255)', 'rgba(176, 217, 235, 255)'],
            data: [260, 100],
        }]
    },
    options : { 
        cutoutPercentage : 60,
         animation : {
             animateScale : true
         }
    }
})

var doughNut4 = document.getElementById('doughnut-chart-4').getContext('2d');
var Chart7 = new Chart(doughNut4,{
    type : 'doughnut',
    data : {
        labels: ['73.1 kW', '73.1 kW'],
        datasets: [{
            label: '',
            backgroundColor: ['rgba(246, 180, 61, 255)','rgba(181, 227, 134, 255)'],
            data: [270, 60],
        }]
    },
    options : { 
        cutoutPercentage : 60,
         animation : {
             animateScale : true
         }
    }
})
var doughNuts = document.getElementById('doughnut-charts').getContext('2d');
var Charts = new Chart(doughNuts,{
    type : 'doughnut',
    data : {
        labels: '',
        datasets: [{
            label: '',
            backgroundColor: ['rgba(246, 180, 61, 255)','rgba(181, 227, 134, 255)'],
            data: [10, 50, 120, 60],
        }]
    },
    options : { 
        cutoutPercentage : 60,
         animation : {
             animateScale : true
         }
    }
})
var pieChart = document.getElementById('pie-chart').getContext('2d');
var pieChart1 = new Chart(pieChart,{
    type : 'pie',
    data : {
        labels: '',
        datasets: [{
            label: '',
            backgroundColor: ['rgba(246, 180, 61, 255)','rgba(181, 227, 134, 255)'],
            data: [10, 50, 120, 60],
        }]
    },
    options : {}
})
var performanceChart = document.getElementById('performance-chart').getContext('2d');
var Chart8 = new Chart(performanceChart,{
    type : 'bar',
    data : {
        labels: ['Jan', 'Feb'],
        datasets : [{
            label: 'a',
            data : data,
            parsing: {
                yAxisKey: 'net'
            }
        },{
            label: 'b',
            data : data,
            parsing: {
                yAxisKey: 'bogs'
            }
        },{
            label: 'c',
            data : data,
            parsing: {
                yAxisKey: 'bigs'
            }
        },]
    },
    options : { 
         animation : {
             animateScale : true
         }
    }
})



var performanceChart = document.querySelector("#performance-chart-2").getContext('2d');

var perf = new Chart(performanceChart, {
    type : 'line',
    data : {
        labels : ['Mar 03','Mar 04','Mar 05','Mar 06','Mar 07','Mar 08','Mar 09','Mar 10','Mar 11','Mar 12','Mar 13'],
        datasets : [{
            label: '',
            data: [6, 2, 10, 6, 3, 10, 2, 8, 1, 8, 0, 9, 2, 9, 1, 7, 2, 9, 3, 10, 8],
            fill: false,
            borderWidth: 3,
            borderColor: 'rgba(243,180,66,255)',
            tension: 0.5,
            pointBorderWidth: 3,
            pointBorderColor: 'rgba(243,180,66,255)',
            order : 1
        },
        {
            label: '',
            data: [6.5, 2.5, 10.5, 6.5, 3.5, 10.5, 2.5, 8.5, 1.5, 8.5, 0.5, 9.5, 2.5, 9.5, 1.5, 7.5, 2.5, 9.5, 3.5, 10.5, 8.5],
            fill: false,
            borderWidth: 3,
            borderColor: 'rgba(175,218,236,255)',
            tension: 0.5,
            pointBorderWidth: 3,
            pointBorderColor: 'rgba(175,218,236,255)',
            order : 2
        }],
    },
    options: {
        plugins: {
            legend: {
              display: false
            }
        },
        scales: {
            xAxes: [{
               gridLines: {
                  display: true
               }
            }],
            yAxes: [{
               ticks: {
                  beginAtZero: true, 
               },
               gridLines: {
                  display: true
               }
            }]
       },
        tooltips: {
            callbacks: {
               label: function(tooltipItem) {
                      return tooltipItem.yLabel;
               }
            }
        },
        responsive : true,
    }
})
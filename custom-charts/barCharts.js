var barChart1 = document.querySelector('#bar-chart-1').getContext('2d');
let grad1 = barChart1.createLinearGradient(0,0,500,0);
grad1.addColorStop(0, "rgba(169,220,240,255)");
grad1.addColorStop(1, "rgba(81,182,226,255)");

var Bar1 = new Chart(barChart1, {
    type: 'bar',
    data : {
        labels: ['','','','','','','','',''],
        datasets: [{
            label : '',
            data: [4, 6, 9, 7, 6, 0.5, 3, 5, 10],
            backgroundColor: grad1
        }]
    },
    options :{
        plugins: {
            legend: {
              display: false
            }
        },
    }
})
var barChart2 = document.querySelector('#bar-chart-2').getContext('2d');
let grad2 = barChart2.createLinearGradient(0,0,500,0);
grad2.addColorStop(0, "rgba(175,199,241,255)");
grad2.addColorStop(1, "rgba(99,142,228,255)");

var Bar2 = new Chart(barChart2, {
    type: 'bar',
    data : {
        labels: ['','','','','','','','',''],
        datasets: [{
            label : '',
            data: [7, 3, 3, 7, 4, 6, 2, 6, 9],
            backgroundColor: grad2
        }]
    },
    options :{
        plugins: {
            legend: {
              display: false
            }
        },
    }
})

var barChart3 = document.querySelector('#bar-chart-3').getContext('2d');
let grad3 = barChart3.createLinearGradient(0,0,500,0);
grad3.addColorStop(0, "rgba(181,179,240,255)");
grad3.addColorStop(1, "rgba(110,101,227,255)");;

var Bar3 = new Chart(barChart3, {
    type: 'bar',
    data : {
        labels: ['','','','','','','','',''],
        datasets: [{
            label : '',
            data: [5, 5, 7, 7, 3, 2, 9, 4, 3.5],
            backgroundColor: grad3
        }]
    },
    options :{
        plugins: {
            legend: {
              display: false
            }
        },
    }
})

var barChart4 = document.querySelector('#bar-chart-4').getContext('2d');
let grad4 = barChart4.createLinearGradient(0,0,500,0);
grad4.addColorStop(0, "rgba(195,174,241,255)");
grad4.addColorStop(1, "rgba(135,83,225,255)");;

var Bar4 = new Chart(barChart4, {
    type: 'bar',
    data : {
        labels: ['','','','','','','','',''],
        datasets: [{
            label : '',
            data: [3, 4, 5, 5, 8, 5, 4, 8, 8],
            backgroundColor: grad4
        }]
    },
    options :{
        plugins: {
            legend: {
              display: false
            }
        },
    }
})

const canvas = document.querySelector('#cycle');

let gradientColor; 

canvas.style.backgroundColor = 'whitesmoke';

const ctx = canvas.getContext('2d');
ctx.clearRect(0, 0, canvas.width, canvas.height);


setTimeout(function(){
    ctx.fillStyle = "rgba(252,166,19,255)"
    ctx.font = '100px FontAwesome'
    ctx.fillText("\uf241",320,120)

    ctx.fillStyle = "rgba(116,116,116,255)"
    ctx.font = '15px Roboto, sans-serif'
    ctx.fillText("Battery",320,40)
    ctx.font = '15px Roboto, sans-serif'
    ctx.fillText("-0.4 kW",390,40)

    ctx.fillStyle = "rgba(253,158,0,255)"
    ctx.font = '70px FontAwesome'
    ctx.fillText("\uf5ba",80,300)
    ctx.fillStyle = "rgba(116,116,116,255)"
    ctx.font = '15px Roboto, sans-serif'
    ctx.fillText("Utility/Grid",70,330)
    ctx.font = '15px Roboto, sans-serif'
    ctx.fillText("73.1 kW",150,330)

    ctx.fillStyle = "rgba(128,205,41,255)"
    ctx.font = '70px FontAwesome'
    ctx.fillText("\uf275",590,300)

    ctx.fillStyle = "rgba(116,116,116,255)"
    ctx.font = '15px Roboto, sans-serif'
    ctx.fillText("Load",590,330)
    ctx.font = '15px Roboto, sans-serif'
    ctx.fillText("-0.4 kW",640,330)
},1000);

gradientColor = ctx.createLinearGradient(0,0,600,0);
gradientColor.addColorStop(0, "rgba(253,158,0,255)");
gradientColor.addColorStop(0.5, "rgba(253,158,0,255)");
gradientColor.addColorStop(1, "rgb(253, 238, 212)"); 
gradientColor1 = ctx.createLinearGradient(0,0,1000,0);
gradientColor1.addColorStop(0, "rgba(253,158,0, 0.5)");

ctx.fillStyle = gradientColor;
ctx.strokeStyle = gradientColor;

ctx.beginPath();
ctx.moveTo(200,265);
ctx.lineWidth = 8
ctx.lineTo(550,265);
ctx.stroke()
ctx.closePath();

ctx.beginPath();
ctx.moveTo(200,255);
ctx.lineTo(200,275);
ctx.lineTo(180, 265)
ctx.lineTo(200,255);
ctx.fill()
ctx.closePath();

ctx.beginPath();
ctx.moveTo(550,255);
ctx.lineTo(570, 265)
ctx.lineTo(550,275);
ctx.lineTo(550,255);
ctx.fill()
ctx.closePath();

ctx.beginPath();
ctx.fillStyle = gradientColor1
ctx.arc(220, 265, 13, 0, Math.PI*2)
ctx.fill()
ctx.closePath();


ctx.beginPath();
ctx.arc(240, 265, 10, 0, Math.PI*2)
ctx.fill()
ctx.closePath();

ctx.beginPath();
ctx.arc(255, 265, 7, 0, Math.PI*2)
ctx.fill()
ctx.closePath();

ctx.beginPath()
ctx.arcTo(20, 30, 30, 20, 10)
ctx.fill()

// Bezier Arcs
ctx.lineWidth = 6
gradientColor4 = ctx.createLinearGradient(0,0,100,0);
gradientColor4.addColorStop(0, "rgb(206, 235, 177)"); 
gradientColor4.addColorStop(0.5, "rgba(181, 227, 134, 255)");
gradientColor4.addColorStop(1, "rgba(181, 227, 134, 255)");
gradientColor5 = ctx.createLinearGradient(0,0,100,0);
gradientColor5.addColorStop(1, "rgba(181, 227, 134, 0.5)");

ctx.fillStyle = gradientColor4;
ctx.strokeStyle = gradientColor4;
ctx.beginPath()
ctx.arc(480,230,150,0, (270*Math.PI)/180,true);
ctx.stroke();
ctx.closePath();

ctx.beginPath()
ctx.lineWidth = 2
ctx.strokeStyle = 'gray'
ctx.arc(260,230,150,820,30);
ctx.stroke();
ctx.closePath();

ctx.lineWidth = 6
ctx.beginPath();
ctx.moveTo(480,90);
ctx.lineTo(480,70);
ctx.lineTo(460,80);
ctx.lineTo(480,90);
ctx.fill()
ctx.closePath();

ctx.beginPath();
ctx.moveTo(620,230);
ctx.lineTo(640,230);
ctx.lineTo(630,250);
ctx.lineTo(620,230);
ctx.fill()
ctx.closePath();

ctx.fillStyle = gradientColor5;
ctx.arc(629, 210, 11, 0, Math.PI*2)
ctx.fill()
ctx.closePath();

ctx.beginPath();
ctx.arc(625, 193, 9, 0, Math.PI*2)
ctx.fill()
ctx.closePath();

ctx.beginPath();
ctx.arc(621, 179, 7, 0, Math.PI*2)
ctx.fill()
ctx.closePath();

gradientColor2 = ctx.createLinearGradient(0,0,600,0);
gradientColor2.addColorStop(0, "rgba(176,217,235,255)");
gradientColor2.addColorStop(0.5, "rgba(176,217,235,255)");
gradientColor2.addColorStop(1, "rgb(220, 240, 248)"); 

ctx.fillStyle = gradientColor2;
ctx.strokeStyle = gradientColor2;
ctx.beginPath()
ctx.arc(380,370,260,0, Math.PI);
ctx.stroke();



ctx.beginPath();
ctx.moveTo(110,370);
ctx.lineTo(130, 370)
ctx.lineTo(120,350);
ctx.lineTo(110,370);
ctx.fill()
ctx.closePath();

ctx.beginPath();
ctx.moveTo(630,370);
ctx.lineTo(650, 370)
ctx.lineTo(640,350);
ctx.lineTo(630,370);
ctx.fill()
ctx.closePath();

gradientColor3 = ctx.createLinearGradient(0,0,600,0);
gradientColor3.addColorStop(0, "rgba(176,217,235,0.5)");

ctx.fillStyle = gradientColor3;
ctx.strokeStyle = gradientColor3
ctx.arc(121, 390, 11, 0, Math.PI*2)
ctx.fill()
ctx.closePath();


ctx.beginPath();
ctx.arc(123, 408, 9, 0, Math.PI*2)
ctx.fill()
ctx.closePath();


ctx.beginPath();
ctx.arc(124-.5, 422, 7, 0, Math.PI*2)
ctx.fill()
ctx.closePath();

function toggleMenu(){
    let toggler = document.querySelector('.toggler');
    let togglerIcon = document.querySelector('.toggler-icon');
    let navigation = document.querySelector('.navigation');
    let mainContent = document.querySelector('.main-content');
    
    toggler.classList.toggle('active');
    navigation.classList.toggle('active');
    mainContent.classList.toggle('active');
    togglerIcon.classList.toggle('fa-chevron-right')
}
function mobileToggleMenu(){
    let toggler = document.querySelector('.mobile-toggler');
    let navigation = document.querySelector('.navigation');
    let mainContent = document.querySelector('.main-content');
    
    toggler.classList.toggle('mobile-active');
    navigation.classList.toggle('mobile-active');
    mainContent.classList.toggle('mobile-active');
}